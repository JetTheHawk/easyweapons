﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR;

public class PhotonPlayerManager : MonoBehaviour
{
    public static List<PhotonPlayerManager> PhotonPlayers = new List<PhotonPlayerManager>();

    public PhotonView photonView = null;

    public CharacterController charController;
    public OVRPlayerController ovrPlayer;
    public OVRSceneSampleController ovrSceneSampleController;
    public OVRDebugInfo ovrDebugInfo;
    public CharacterCameraConstraint characterCameraConstraint;
    public OVRManager ovrManager;
    public OVRCameraRig ovrCameraRig;
    public GameObject locomotionController;
    public Camera CenterEyeCamera;
    public WeaponSystem weapon;

    private void OnEnable()
    {
        // Track this HumanoidManager in Static Humanoids List
        PhotonPlayers.Add(this);

        // Disable Components Specific to Local Player if not Local Player
        if (!IsLocalPlayer())
        {

            ovrManager.enabled = false;
            GetComponentInChildren<AudioListener>().gameObject.SetActive(false);
            Camera[] cams = GetComponentsInChildren<Camera>();
            for (int i = 0; i < cams.Length; i++)
            {
                cams[i].enabled = false;
            }

            charController.enabled = false;
            ovrPlayer.enabled = false;
            ovrSceneSampleController.enabled = false;
            ovrDebugInfo.enabled = false;
            characterCameraConstraint.enabled = false;

            if (ovrManager == null)
            {
                ovrManager = GetComponentInChildren<OVRManager>();
            }
            if (ovrCameraRig == null)
            {
                ovrCameraRig = GetComponentInChildren<OVRCameraRig>();
            }
            if (locomotionController == null)
            {
                locomotionController = GetComponentInChildren<LocomotionController>().gameObject;
            }
            
            locomotionController.SetActive(false);

            ////HandednessControllerObject[] controllers = GetComponentsInChildren<HandednessControllerObject>(true);
            ////for (int i = 0; i < controllers.Length; i++)
            ////{
            ////    controllers[i].enabled = false;
            ////}
            ////GetComponent<SteamVR_Ears>().enabled = false;
            ////SteamVR_Behaviour_Pose[] Poses = GetComponentsInChildren<SteamVR_Behaviour_Pose>();
            ////for (int i = 0; i < Poses.Length; i++)
            ////{
            ////    Poses[i].enabled = false;
            ////}
            //locomotionController.SetActive(false);
        }


        // Place player at assigned bay based on Photon ID
        //if (PhotonNetwork.LocalPlayer.ActorNumber > BayManager.Singleton.Bays.Count)
        //{
        //    Debug.Log("Photon Actor ID is greater than Available Bays!");
        //    return;
        //}

        //Transform PlayerBay = BayManager.Singleton.Bays[PhotonNetwork.LocalPlayer.ActorNumber - 1];
        //if (PlayerBay != null)
        {
            //transform.SetParent(PlayerBay, false);
            //transform.localPosition = new Vector3(0, 0, 0.5f);
            //transform.localEulerAngles = new Vector3(0, 180, 0);
            //transform.position = new Vector3(PlayerBay.position.x, PlayerBay.position.y + 2, PlayerBay.position.x);
            //transform.localEulerAngles = new Vector3(0, 180, 0);
        }

        if (IsLocalPlayer())
        {


            if (ovrManager == null)
            {
                ovrManager = GetComponentInChildren<OVRManager>();
            }
            if (ovrCameraRig == null)
            {
                ovrCameraRig = GetComponentInChildren<OVRCameraRig>();
            }
            if (locomotionController == null)
            {
                locomotionController = GetComponentInChildren<LocomotionController>().gameObject;
            }
            ovrManager.enabled = true;
            ovrCameraRig.enabled = true;
            CenterEyeCamera.enabled = true;
            charController.enabled = true;
            ovrPlayer.enabled = true;
            ovrSceneSampleController.enabled = true;
            ovrDebugInfo.enabled = true;
            characterCameraConstraint.enabled = true;
            Weapon[] playerWeapons = weapon.GetComponentsInChildren<Weapon>(true);
            foreach (Weapon gun in playerWeapons){
                gun.enabled = true;
            }
            weapon.enabled = true;

            locomotionController.SetActive(true);
        }
    }

    private void OnDisable()
    {
        // STOP Tracking this HumanoidManager in Static Humanoids List
        PhotonPlayers.Remove(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        name = name + "_" + photonView.ownerId;

        if (IsLocalPlayer())
            name = name + "_Local";
        else
            name = name + "_Network";
    }

    bool IsLocalPlayer()
    {
        if (PhotonNetwork.LocalPlayer != null && photonView != null)
            return PhotonNetwork.LocalPlayer.ActorNumber == photonView.ownerId; // Note: May need to be UserID instead of Photon ActorNumber
        else
        {
            Debug.LogError("PhotonNetwork Player is NULL and or Photon View is NULL!");
            return false;
        }
    }

    //Vector3 latestPosition = Vector3.zero;
    //Vector3 latestRotation = Vector3.zero;
    //void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.IsWriting)
    //    {
    //        // Send Handedness
    //        stream.SendNext(HandednessManager.IsRightHand);

    //        // Send Position & Rotation
    //        stream.SendNext(transform.position);
    //        stream.SendNext(transform.eulerAngles);

    //        // Send Animation States
    //        //stream.SendNext(anim.GetFloat("moveX"));
    //        //stream.SendNext(anim.GetFloat("moveY"));
    //        stream.SendNext(anim.GetFloat("speed"));
    //    }
    //    else
    //    {
    //        // Recieve Position & Rotation
    //        latestPosition = (Vector3)stream.ReceiveNext();
    //        latestRotation = (Vector3)stream.ReceiveNext();

    //        // Recieve Animation States
    //        //anim.SetFloat("moveX", (float)stream.ReceiveNext());
    //        //anim.SetFloat("moveY", (float)stream.ReceiveNext());
    //        anim.SetFloat("speed", (float)stream.ReceiveNext());
    //    }
    //}
}
