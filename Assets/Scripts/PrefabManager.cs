﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    private static List<PrefabManager> PrefabManagers = new List<PrefabManager>();

    public string PrefabName = "";
    public string PrefabNameTag = "";
    public bool InitializeAllEnabled = false;
    public bool IncreasePoolSizeAsNeeded = true;
    public bool IsNetworked = true;
    public GameObject NonNetworkedPrefab;

    private Dictionary<Transform, bool> Pool = new Dictionary<Transform, bool>();
    //private List<Transform> Pool = new List<Transform>();

    //BallFollower BFollower;

    private void Awake()
    {
        // Add PrefabManager to List of PrefabManagers
        PrefabManagers.Add(this);

        // Add all children of Gameobject script is attached to, add to pool, and disable
        // Note: This is temp functionality
        for (int i = 0; i < transform.childCount; i++)
        {
            Pool.Add(transform.GetChild(i), false);
            transform.GetChild(i).gameObject.SetActive(InitializeAllEnabled);
        }
    }

    public static PrefabManager GetPrefabManager(string prefabNameTag)
    {
        for (int i = 0; i < PrefabManagers.Count; i++)
        {
            if (PrefabManagers[i].PrefabNameTag == prefabNameTag)
                return PrefabManagers[i];
        }

        Debug.LogError("PrefabManager.GetPrefabManager(): No Prefab Managers with " + prefabNameTag + " tag!");
        return null;
    }

    public Transform requestPrefab()
    {
        for (int i = 0; i < Pool.Count; i++)
        {
            if (!Pool.Values.ElementAt(i))
            {
                Pool[Pool.Keys.ElementAt(i)] = true;

                if (!InitializeAllEnabled)
                    Pool.Keys.ElementAt(i).gameObject.SetActive(true);

                return Pool.Keys.ElementAt(i);
            }
        }

        if (IncreasePoolSizeAsNeeded)
        {
            if (!string.IsNullOrEmpty(PrefabName))
            {
                GameObject PlayerPrefab = new GameObject();
                //TODO: Instantiate a new Prefab and add to the Pool
                if (IsNetworked)
                {
                    PlayerPrefab = PhotonNetwork.Instantiate(PrefabName, Vector3.zero, Quaternion.identity, 0);
                }
                else
                {
                    PlayerPrefab = GameObject.Instantiate(NonNetworkedPrefab, Vector3.zero, Quaternion.identity);
                }
                
                Pool.Add(PlayerPrefab.transform, true);
                return PlayerPrefab.transform;
            }
            else
            {
                Debug.LogError("PrefabManager.requestPrefab(): PrefabName is NULL or EMPTY!");
            }
        }

        return null;
    }

    public void returnPrefab(Transform prefab)
    {
        if (prefab != null)
        {
            Pool[prefab] = false;

            if (!InitializeAllEnabled)
                prefab.gameObject.SetActive(false);
        }
        else
            Debug.LogWarning("PrefabManager.returnPrefab(): prefab parameter is NULL!");
    }

}
