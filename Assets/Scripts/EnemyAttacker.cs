﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAttacker : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform CurrentTarget;
    private bool isWalking = false;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(targetPostionUpdater());
        if(CurrentTarget == null)
        {
            CurrentTarget = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    public void SetEnemySteeringTarget(Transform newTarget)
    {
        CurrentTarget = newTarget;
    }

    public void StopTargeterCoroutine()
    {
        StopCoroutine(targetPostionUpdater());
    }
    // Update is called once per frame
    public IEnumerator targetPostionUpdater()
    {
        while (true)
        {
            if (CurrentTarget == null)
            {
                if(GameObject.FindGameObjectWithTag("Player") != null)
                    CurrentTarget = GameObject.FindGameObjectWithTag("Player").transform;
            }
            if (agent == null || agent.enabled == false)
            {
                yield break;
            }
            if (CurrentTarget != null)
            {
                agent.SetDestination(CurrentTarget.position);
            }

            if(agent.velocity.magnitude != 0 && isWalking == false)
            {
                isWalking = true;
                if(anim != null)
                {
                    anim.SetBool("IsWalking", true);
                }
            }
            else if(agent.velocity.magnitude == 0 && isWalking == true)
            {
                isWalking = false;
                if (anim != null)
                {
                    anim.SetBool("IsWalking", false);
                }
            }

            yield return new WaitForSeconds(1);
        }

    }

    private void OnDisable()
    {
        StopCoroutine(targetPostionUpdater());
    }
}
