﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    string gameVersion = "SGR_0.0.1";

    public List<Transform> spawnPoints = new List<Transform>();

    //public Camera standbyCam;

    PhotonView photonView = null;

    public string PlayerPrefabName = "";

    public Camera StagingCamera;

    private void Awake()
    {

    }

    // Use this for initialization
    void Start()
    {
        //spawnSpots = GameObject.FindObjectsOfType<SpawnSpot>();

        Connect();
    }

    private void Connect()
    {
        if (!PhotonNetwork.IsConnected)
        {
            Debug.Log("Connecting...");
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    private void OnGUI()
    {
        if (PhotonNetwork.NetworkClientState != Photon.Realtime.ClientState.Joined)
            GUILayout.Label(PhotonNetwork.NetworkClientState.ToString());
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        Debug.Log("Successfully connected to master");
        Debug.Log("Attempting to join random room");
        PhotonNetwork.JoinRandomRoom();

    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);

        Debug.Log("Random room join failed. " + message +" Creating new room...");
        PhotonNetwork.CreateRoom("room1", null, null);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Debug.Log("OnJoinedRoom");
        //standbyCam.gameObject.SetActive(false);
        SpawnMyPlayer();

        photonView = PhotonView.Get(this);
    }

    //ClubCube LeftClubCube;
    //ClubCube RightClubCube;

    void SpawnMyPlayer()
    {
        GameObject PlayerPrefab = PhotonNetwork.Instantiate(PlayerPrefabName, spawnPoints[0].position, Quaternion.identity, 0);
        

        //SimpleRoundOfGolfManager.Singleton.AddPlayerToGame(PlayerPrefab.GetComponentInChildren<GolfPlayer>());
        // Assign Player Controllers to HandednessManager
        
        //SteamVR_Behaviour_Pose[] Controllers = PlayerPrefab.GetComponentsInChildren<SteamVR_Behaviour_Pose>();
        //HandednessControllerObject[] clubs = PlayerPrefab.GetComponentsInChildren<HandednessControllerObject>(true);
        //Debug.Log("clubs found "+clubs.Length);
        //for (int i = 0; i < clubs.Length; i++)
        //{
        //    if (clubs[i].transform.name.ToLower().Contains("left"))
        //    {
        //        HandednessManager.Singleton.LeftHandController = clubs[i].transform;
        //    }
        //    else if (clubs[i].transform.name.ToLower().Contains("right"))
        //    {
        //        HandednessManager.Singleton.RightHandController = clubs[i].transform;
        //    }
        //    //if (Controllers[i].name.ToLower().Contains("left"))
        //    //    HandednessManager.Singleton.LeftHandController = Controllers[i].transform;
        //    //else if (Controllers[i].name.ToLower().Contains("right"))
        //    //    HandednessManager.Singleton.RightHandController = Controllers[i].transform;
        //}

        //LeftClubCube = HandednessManager.Singleton.LeftHandController.GetComponentInChildren<ClubCube>();
        //RightClubCube = HandednessManager.Singleton.RightHandController.GetComponentInChildren<ClubCube>();

        //// Assign Player Objects to HandednessManager Lists
        //for (int i = 0; i < HandednessManager.Singleton.LeftHandController.childCount; i++)
        //{
        //    HandednessManager.Singleton.LeftHandObjects.Add(HandednessManager.Singleton.LeftHandController.GetChild(i).transform);
        //}
        //for (int i = 0; i < HandednessManager.Singleton.RightHandController.childCount; i++)
        //{
        //    HandednessManager.Singleton.RightHandObjects.Add(HandednessManager.Singleton.RightHandController.GetChild(i).transform);
        //}

        //// Place player at assigned bay based on Photon ID
        //if (PhotonNetwork.LocalPlayer.ActorNumber > BayManager.Singleton.Bays.Count)
        //{
        //    Debug.Log("Photon Actor ID is greater than Available Bays!");
        //    return;
        //}

        //Transform PlayerBay = BayManager.Singleton.Bays[PhotonNetwork.LocalPlayer.ActorNumber - 1];
        //if (PlayerBay != null)
        //{
        //    BallSpawner[] TeePositions = PlayerBay.GetComponentsInChildren<BallSpawner>(true);
        //    if (TeePositions != null && TeePositions.Length == 2)
        //    {
        //        for (int i = 0; i < TeePositions.Length; i++)
        //        {
        //            TeePositions[i].gameObject.SetActive(true);

        //            if (TeePositions[i].name.ToLower().Contains("left"))
        //                HandednessManager.Singleton.LeftHandObjects.Add(TeePositions[i].transform);
        //            else if (TeePositions[i].name.ToLower().Contains("right"))
        //                HandednessManager.Singleton.RightHandObjects.Add(TeePositions[i].transform);
        //        }
        //    }
        //    else
        //    {
        //        Debug.LogError("NetworkManger.SpawnPlayer(): Player Bay is missing some or all of their Tee Positions!");
        //    }
        //}

        // Initialize Handedeness to RIGHT HAND
        Debug.Log("NetworkManager.SpawnPlayer(): HandednessObjects Assigned. Initialize to Right Hand.");
        //HandednessManager.Singleton.ToggleHandednessObjects(true);
        //StartCoroutine(WaitForClubHeadFollowersThenAdd());
    }

    //IEnumerator WaitForClubHeadFollowersThenAdd()
    //{
    //    while (true)
    //    {
    //        yield return new WaitForSeconds(1f);

    //        if (LeftClubCube._clubHeadFollowerPrefab.transform != null || RightClubCube._clubHeadFollowerPrefab.transform != null)
    //        {
    //            HandednessManager.Singleton.LeftHandObjects.Add(LeftClubCube.spawnedFollower.transform);
    //            HandednessManager.Singleton.RightHandObjects.Add(RightClubCube.spawnedFollower.transform);
    //            HandednessManager.Singleton.ToggleHandednessObjects(true);
    //            yield break;
    //        }
    //    }
    //}

    //void DisableActiveAudioListeners()
    //{
    //    AudioListener[] listeners = Object.FindObjectsOfType<AudioListener>();

    //    for (int i = 0; i < listeners.Length; i++)
    //    {
    //        listeners[i].enabled = false;
    //    }
    //}

    // Update is called once per frame
    void Update()
    {
        //if (photonView != null)
        //{
        //    photonView.RPC("UpdateEnemyPosition", PhotonTargets.Others, )
        //}
    }

    [PunRPC]
    void UpdateEnemyPosition(Vector3 position)
    {

    }

    void OnPhotonSearializeView()
    {

    }
}
